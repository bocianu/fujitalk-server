const config = require('./config.js');
const CNST = require('./constants.js');

const db = require('better-sqlite3')(config.DB_file, config.sqliteOptions);
const md5 = require('md5');

const getUserByKey = key => {
    return db.prepare('SELECT * FROM users WHERE key = ?').get(key);
}

const getUserById = id => {
    return db.prepare('SELECT * FROM users WHERE id = ?').get(id);
}

const getUserByNick = nick => {
    return db.prepare('SELECT * FROM users WHERE nick = ?').get(nick);
}

const getUserByNickPass = (nick, pass) => {
    return db.prepare('SELECT * FROM users WHERE nick = ? AND pass = ?').get(nick, md5(pass));
}

const updateUserActivity = nick => {
    return db.prepare('UPDATE users SET lastActivity = CURRENT_TIMESTAMP WHERE nick = ?').run(nick);
}

const setUserProp = (id,propname,val) => {
    return db.prepare(`UPDATE users SET ${propname} = ${val} WHERE id = ${id}`).run();
}

const purgeChannel = cname => {
    const c = getChannelByName(cname);
    if (c) {
        db.prepare(`delete FROM channelMsg WHERE channelId = ${c.id}`).run();
        return true;
    } 
    return false;
}

const removeChannel = cname => {
    const c = getChannelByName(cname);
    if (c) {
        db.prepare(`delete FROM channels WHERE name = '${cname}'`).run();
        return true;
    } 
    return false;
}


const generateUniqueKey = () => {
    const elems = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];
    let isInBase = false;
    let key;
    do {
        key = '';
        for (let i = 0; i < 8; i++) {
            key += elems[Math.floor(Math.random()*16)];
        }
        isInBase = getUserByKey(key);
    } while (isInBase)
    return key
}

const newUser  = (nick, pass) => {
    const userKey = generateUniqueKey();
    const insert = db.prepare('INSERT INTO users (nick, pass, key, lastActivity) VALUES (@nick, @pass, @key, CURRENT_TIMESTAMP)');
    
    try {
        insert.run({nick, pass: md5(pass), key: userKey});
    } catch (error) {
        //userError( )
        console.log(error.code);
        return false;
    }

    return getUserByKey(userKey);
}

const addPrivateMsg = msg => {
    const insert = db.prepare('INSERT INTO directMsg (fromId, toId, fromName, body, msgTime) VALUES (@fromId, @toId, @fromName, @body, CURRENT_TIMESTAMP)');
    try {
        insert.run(msg);
    } catch (error) {
        //userError( )
        console.log(error.code);
        return false;
    }
}

const getPrivateLastMsg = (id_a, id_b) => {
    return db.prepare(`SELECT id FROM directMsg WHERE (fromId = ${id_a} AND toId = ${id_b}) OR (fromId = ${id_b} AND toId = ${id_a}) ORDER BY id DESC LIMIT 1`).pluck().get();
}

const addChannelMsg = msg => {
    const insert = db.prepare('INSERT INTO channelMsg (fromId, channelId, fromName, body, msgTime) VALUES (@fromId, @channelId, @fromName, @body, CURRENT_TIMESTAMP)');
    const cid = msg.channelId;
    let result = null;
    try {
        result = insert.run(msg);
    } catch (error) {
        //userError( )
        console.log(error.code);
        return false;
    }
    const lastMsg = result.lastInsertRowid;
    //console.log('last insert id:' + result.lastInsertRowid);
    setChannelLastMsg(cid, lastMsg);
    return lastMsg;
}

const getChannelMsg = mid => {
    return db.prepare(`SELECT * FROM channelMsg WHERE id = ?`).get(mid);
}

const getChannelByName = cname => {
    return db.prepare('SELECT * FROM channels WHERE name = ?').get(cname);
}

const getChannelById = cid => {
    return db.prepare('SELECT * FROM channels WHERE id = ?').get(cid);
}

const getChannelsLastMsg = () => {
    return db.prepare('SELECT id,lastMsg FROM channels').all();
}

const getChannelLastMsg = cid => {
    return db.prepare('SELECT lastMsg FROM channels WHERE id = ? LIMIT 1').pluck().get(cid);
}

const getChannelLines = (cid, lastMsg, showSrvMsg) => {
    const where = (showSrvMsg == 1)?'':'AND fromId IS NOT NULL'
    return db.prepare(`SELECT * FROM channelMsg WHERE channelId = ? AND id > ? ${where} ORDER BY id DESC LIMIT ?`).all(cid, lastMsg, CNST.TAB_LINES);
}

const getPrivateLines = (fromId, toId, lastMsg) => {
    lastMsg = lastMsg==0xffffffff?-1:lastMsg;
    return db.prepare(`SELECT * FROM directMsg WHERE ((fromId = ${fromId} AND toId = ${toId}) OR (fromId = ${toId} AND toId = ${fromId})) AND id > ? ORDER BY id DESC LIMIT ?`).all(lastMsg, CNST.TAB_LINES);
}

const clearUnreadPrivateMessages = (msgId) => {
    return db.prepare(`UPDATE directMsg SET unread = FALSE WHERE id IN (${msgId.join(', ')})`).run()
}

const setChannelLastMsg = (cid, lastId) => {
    return db.prepare('UPDATE channels SET lastMsg = ? WHERE id = ?').run(lastId, cid);
}

const getMostActiveChannels = num => {
    return db.prepare(`SELECT c.name,count(c.id) as cnt FROM channelMsg m LEFT JOIN channels c ON m.channelId = c.id GROUP BY m.channelId ORDER BY cnt DESC LIMIT ?`).all(num);
}

const getUnreadPrivateList= uid => {
    return db.prepare(`SELECT u.nick,count(u.id) as cnt FROM directMsg m LEFT JOIN users u ON m.fromId = u.id WHERE m.toId = ? AND m.unread = TRUE GROUP BY m.fromId ORDER BY cnt DESC`).all(uid);
}

const newChannel = cname => {
    const insert = db.prepare('INSERT INTO channels (name, lastMsg) VALUES (?, 0)');
    try {
        insert.run(cname);
    } catch (error) {
        console.log(error.code);
        return false;
    }
    const channel = getChannelByName(cname);    
    addChannelMsg({
        channelId: channel.id,
        fromId: null,
        fromName: '*',
        body: `channel #${cname} created`
    });
    return channel;
}

module.exports = {
    newUser,
    getUserByNick,
    getUserByNickPass,
    getUserByKey,
    getUserById,
    updateUserActivity,
    setUserProp,

    newChannel,
    getChannelByName,
    getChannelById,
    getChannelLastMsg,
    getChannelLines,
    addChannelMsg,
    getChannelMsg,
    getChannelsLastMsg,

    
    getPrivateLastMsg,
    getPrivateLines,
    addPrivateMsg,
    clearUnreadPrivateMessages,
    getUnreadPrivateList,

    getMostActiveChannels,

    purgeChannel,
    removeChannel

};


