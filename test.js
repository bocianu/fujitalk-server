const ftdb = require('./ftdb.js');

//for (let index = 0; index < 8; index++) { console.log(ftdb.generateUniqueKey()) };


//const newUser = ftdb.newUser('bocian2u', '1234');
//console.log(newUser);

const getCardinalArray = card => {
	let count = 0;
	const arr = new Uint8Array(4);
	let mask = 0xff;
	while (count<4) {
		b = (card & mask) >> (count * 8);
		arr[count] = b;
		count++;
		mask = mask << 8;
	}
	return arr;
}

const bodyCardinal = c => String.fromCharCode(...getCardinalArray(c));


console.log(String.fromCharCode(...getCardinalArray(4294967295)));
