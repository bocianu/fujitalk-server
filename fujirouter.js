const Router = require('koa-router');
const _ = require('lodash');

const ftdb = require('./ftdb.js');
const fujitalk = require('./fujitalk.js');
const { body } = require('koa/lib/response');

var CNST = require('./constants.js');

const router = new Router({
	prefix: ''
});

const DEBUG_AUTH = true;
const DEBUG_SAY = true;
const DEBUG_STATUS = false;
const DEBUG_LINES = false;

router.post('/say/:target', async (ctx, next) => {
	const userKey = ctx.request.headers.userkey;
	const target = ctx.params.target;
	if (DEBUG_SAY) console.log(`/say - ${userKey} -> tab: ${target} -> ${ctx.request.body}`)
	ctx.type = 'application/octet-stream';
	if (!fujitalk.loggedIn(userKey)) {
		console.log(`unknown userKey : ${userKey}`)
		return false;
	}
	const uSession = fujitalk.getSession(userKey);
	ftdb.updateUserActivity(uSession.user.nick);
	if (ctx.request.body[0] == '/') {
		fujitalk.sendCommand(ctx.request.body, userKey, target)
		ctx.response.status = 200;
		await next();
		return;
	}
	if (target == 0) {
		uSession.msg.push('* Talking here has no point.');
		uSession.msg.push('* Better /join channel first.');
	} else {
		fujitalk.sendMessage(ctx.request.body, userKey, target);
	}
	ctx.response.status = 200;
	await next();
});

router.get('/auth/:nick', async (ctx, next) => {
	ctx.type = 'application/octet-stream';
	const userKey = ctx.request.headers.userkey;
	const nick = ctx.params.nick;
	if (DEBUG_AUTH) console.log(`/auth - ${userKey} as ${nick}`)
	const user = ftdb.getUserByKey(userKey);
	if (user && user.nick == nick) {
		ctx.response.status = 200;
		fujitalk.loginUser(user);
		ctx.body = fujitalk.buildBody(CNST.TOKEN_USER, user);
	} else {
		ctx.response.status = 401;
		ctx.body = fujitalk.buildBody(CNST.TOKEN_ERROR, { code: CNST.ERROR_LOGIN_FAILED, msg: 'User Unknown' });
	}
	await next();
});

router.get('/version/:version', async (ctx, next) => {
	ctx.type = 'application/octet-stream';
	const clientVersion = ctx.params.version;
	if (!fujitalk.isVersionOk(clientVersion)) {
		ctx.body = fujitalk.buildBody(CNST.TOKEN_ERROR, { code: CNST.ERROR_VERSION_MISMATCH, msg: 'Client is obsolete!' });
	}
	await next();
});

router.get('/lines/:tab/:last', async (ctx, next) => {
	ctx.type = 'application/octet-stream';
	const userKey = ctx.request.headers.userkey;
	if (!fujitalk.loggedIn(userKey)) {
		console.log(`unknown userKey : ${userKey}`)
		return false;
	}
	const uSession = fujitalk.getSession(userKey);
	const targetTab = Number(ctx.params.tab);
	if (!uSession.tabs[targetTab]) {
		console.log(`unknown tab : ${targetTab}`)
		return false;
	}
	const linesData =  fujitalk.getLinesData(userKey,targetTab,Number(ctx.params.last));

	if (DEBUG_LINES) console.log(`/lines - ${userKey} <- ${linesData.length}`);
	
	ctx.body = linesData?fujitalk.buildBody(CNST.TOKEN_LINES,linesData):'';
	await next();
});

router.get('/register/:nick/:pass', async (ctx, next) => {
	if (DEBUG_AUTH) console.log(`/register - ${ctx.params.nick}:${ctx.params.pass}`)
	let user = ftdb.getUserByNickPass(ctx.params.nick,ctx.params.pass);
	ctx.type = 'application/octet-stream';
	if (user) {
		ctx.response.status = 409;
		ctx.body = fujitalk.buildBody(CNST.TOKEN_ERROR, { code: CNST.ERROR_SIGNUP_FAILED, msg: 'User already exists' });
		await next();
		return false;
	}
	user = ftdb.newUser(ctx.params.nick, ctx.params.pass);
	if (user) {
		ctx.response.status = 200;
		fujitalk.loginUser(user);
		ctx.body = fujitalk.buildBody(CNST.TOKEN_USER, user);
	} else {
		ctx.response.status = 409;
		ctx.body = fujitalk.buildBody(CNST.TOKEN_ERROR, { code: CNST.ERROR_SIGNUP_FAILED, msg: 'Signup failed' });
	}
	await next();
});

router.get('/login/:nick/:pass', async (ctx, next) => {
	ctx.type = 'application/octet-stream';
	user = ftdb.getUserByNickPass(ctx.params.nick, ctx.params.pass);
	if (DEBUG_AUTH) console.log(`/login - ${ctx.params.nick}:${ctx.params.pass}`)
	if (user) {
		ctx.response.status = 200;
		fujitalk.loginUser(user);
		ctx.body = fujitalk.buildBody(CNST.TOKEN_USER, user);
	} else {
		ctx.response.status = 409;
		ctx.body = fujitalk.buildBody(CNST.TOKEN_ERROR, { code: CNST.ERROR_LOGIN_FAILED, msg: 'Invalid credentials' });
	}
	await next();
});body

router.get('/status', async (ctx, next) => {
	ctx.type = 'application/octet-stream';
	const userKey = ctx.request.headers.userkey;
	if (DEBUG_STATUS) console.log(`/status - ${userKey}`);
	if (!fujitalk.loggedIn(userKey)) {
		ctx.response.status = 401;
		ctx.body = fujitalk.buildBody(CNST.TOKEN_ERROR, { code: CNST.ERROR_UNAUTHORIZED, msg: 'Unauthorized' });
		return false;
	}
	ctx.response.status = 200;
	const status = fujitalk.getStatus(userKey);
	ctx.body = fujitalk.buildBody(CNST.TOKEN_STATUS, status);
	await next();
});

module.exports = router;
