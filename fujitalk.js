const _ = require('lodash');

var CNST = require('./constants');
const ftdb = require('./ftdb.js');
const config = require('./config.js');
const fujihelp = require('./fujihelp.js');
const ftdata = require('./fujicache.js');


const tab0 = { name: '*', id: null, lastMsg: 0 }
const sessions = {};
const session = {
	user: null,
	forceTabOpen: CNST.NONE,
	tabs: [tab0],
	msg: []
}

const loggedIn = userKey => {
	return (sessions[userKey])
}

const isVersionOk = clientVersion => {
	const cver = clientVersion.split('.').map(x => Number(x));
	const minver = CNST.REQUIRED_CLIENT_VERSION.split('.').map(x => Number(x));
	if (cver[0] < minver[0]) return false;
	if (cver[0] > minver[0]) return true;
	if (cver[1] < minver[1]) return false;
	if (cver[1] > minver[1]) return true;
	if (cver[2] < minver[2]) return false;
	return true;
}

const loginUser = user => {
	user.lastActivity = ftdata.nowInSec();
	const uSession = sessions[user.key];
	if (uSession) {
		uSession.user = user;
	} else {
		sessions[user.key] = _.assign(_.cloneDeep(session), { user });
	}
	sessions[user.key].msg.push(...config.welcomeMsg);
}

const FindNameInTabs = (name, tabs) => {
	let tabnum = CNST.NONE;
	tabs.forEach((tab, i) => {
		if (tab.name.toUpperCase() == name.toUpperCase()) {
			tabnum = i;
		}
	});
	return tabnum;
}

const closeTab = (userKey, name) => {
	const uSession = sessions[userKey];
	let newTab = CNST.NONE;
	if (uSession) {
		newTab = FindNameInTabs(name, uSession.tabs);
		if ((newTab == CNST.NONE) || (newTab == 0)) {
			userError(userKey, CNST.ERROR_NO_SUCH_WINDOW, `No such window: ${name}`);
			return false;
		}
		const private = (name[0] == '@');
		if (!private) {
			let dbdata = ftdb.getChannelByName(name);
			ftdata.addChannelMsg(uSession, {
				channelId: dbdata.id,
				fromId: null,
				fromName: '*',
				body: `user ${uSession.user.nick} has left.`
			});
		}
		uSession.tabs.splice(newTab, 1);
		newTab = 0;
	}
	//console.log(uSession);
	uSession.forceTabOpen = newTab;
}


const addTab = (userKey, name) => {
	const uSession = sessions[userKey];
	let private = false;
	let dbname = name;
	let newTab = CNST.NONE;

	if (uSession) {

		newTab = FindNameInTabs(name, uSession.tabs);

		if (newTab == CNST.NONE) {

			if (uSession.tabs.length > CNST.MAX_TAB_LIMIT) {
				userError(userKey, CNST.ERROR_TAB_LIMIT, `Maximum number of tabs (${CNST.MAX_TAB_LIMIT}) opened`);
				return false;
			}

			if (name[0] == '@') {
				private = true;
				dbname = name.substring(1);
			}
			let dbdata = private ? ftdb.getUserByNick(dbname) : ftdb.getChannelByName(dbname);

			if (private && !dbdata) {
				userError(userKey, CNST.ERROR_USER_UNKNOWN, `User ${dbname} not found.`);
				return false;
			}

			if (!dbdata) {
				dbdata = ftdb.newChannel(dbname);
			}

			//console.log(dbdata);

			const tab = {
				id: dbdata.id,
				name,
				lastMsg: 0
			}
			uSession.tabs.push(tab);
			if (!private) {
				ftdata.addChannelMsg(uSession, {
					channelId: dbdata.id,
					fromId: null,
					fromName: '*',
					body: `user ${uSession.user.nick} has joined.`
				});
			}
			newTab = uSession.tabs.length - 1;
		}
	}
	//console.log(uSession);
	uSession.forceTabOpen = newTab;
}

const userError = (userKey, code, msg) => {
	const uSession = sessions[userKey];
	if (uSession) {
		uSession.msg.push(`! ERROR ${code} : ${msg}`)
	};
}

const getCardinalArray = card => {
	let count = 0;
	const arr = new Uint8Array(4);
	let mask = 0xff;
	while (count < 4) {
		b = (card & mask) >> (count * 8);
		arr[count] = b;
		count++;
		mask = mask << 8;
	}
	return Array.from(arr);
}

const bodyByte = b => b & 0xFF;
const bodyCardinal = c => getCardinalArray(c);
const bodyString = (s, ssize = s.length) => [s.length, ...getCharCodes(s), ...Array(ssize - s.length).fill(32)];
const bodyLine = (s, ssize) => [...getCharCodes(s), ...Array(ssize - s.length).fill(32)];

const broadcast = msg => {
	for (s in sessions) {
		sessions[s].msg.push(msg);
	};
}

const closeAllSessionsTab = cname => {
	for (s in sessions) {
		const newTab = FindNameInTabs(cname, sessions[s].tabs);
		if ((newTab == CNST.NONE) || (newTab == 0)) {
			return false;
		}
		sessions[s].tabs.splice(newTab, 1);
	}
	sessions[s].forceTabOpen = 0;
};

const getOutputLinesFromDB = dbLines => {
	let lines = [];
	let dbline = 0;
	while (dbline < dbLines.length) {
		const line = dbLines[dbline++];
		let msg = line.body;
		if (msg) {
			const srv = (line.fromId == null);
			let line1len = srv ? 40 - 1 - line.fromName.length : 40 - 3 - line.fromName.length;
			lines.push({
				id: line.id,
				body: srv ? `${line.fromName} ${msg.substring(0, line1len)}` : `<${line.fromName}> ${msg.substring(0, line1len)}`
			})
			msg = msg.substring(line1len);
			while (msg.length > 0) {
				lines.push({
					id: line.id,
					body: msg.substring(0, 40)
				})
				msg = msg.substring(40);
			}
		}
	}
	lines = lines.reverse().splice(0, CNST.TAB_LINES).reverse();
	//	console.log(lines);
	return lines;
}

const getPrivateLines = (fromId, toId, lastMsg) => {
	const dbLines = ftdb.getPrivateLines(fromId, toId, lastMsg).reverse();
	const nlines = dbLines.filter(l => { return (l.toId == fromId) && (l.unread == 1) }).map(l => l.id);
	ftdb.clearUnreadPrivateMessages(nlines);
	return getOutputLinesFromDB(dbLines);
}

const getUnreadPrivateList = uid => {
	const users = ftdb.getUnreadPrivateList(uid);
	return users.map(u => `${u.nick}(${u.cnt})`);
}

const getChannelLines = (uSession, chanelId, lastMsg) => {
	const dbLines = ftdata.getChannelLines(uSession, chanelId, lastMsg).reverse();
	//console.log(dbLines);
	return getOutputLinesFromDB(dbLines);
}

const getServerLines = (userKey) => {
	const lines = [];
	const uSession = sessions[userKey];
	while (uSession.msg.length > 0) {
		msg = uSession.msg.shift();
		if (msg) {
			do {
				lines.push({
					id: 0,
					body: msg.substring(0, 40)
				})
				msg = msg.substring(40);
			} while (msg.length > 0);
		}
	}
	//console.log(lines);
	return lines;
}

const getLinesData = (userKey, targetTab, lastMsg) => {
	const uSession = getSession(userKey);
	const targetId = uSession.tabs[targetTab].id;
	const private = (uSession.tabs[targetTab].name[0] == '@');
	const lines = (targetTab == 0) ? getServerLines(userKey, lastMsg) : (private ? getPrivateLines(uSession.user.id, targetId, lastMsg) : getChannelLines(uSession, targetId, lastMsg));

	if (lines.length == 0) {
		console.log(`unexpected empty response for lines`);
		console.log(`user: ${uSession.user.nick} tab: ${targetTab} lastmsg: ${lastMsg}`);
		console.log(uSession);
		return false;
	}

	return {
		targetTab,
		lineCount: lines.length,
		lastLine: lines[lines.length - 1].id,
		lines
	};

}

const getMostActiveChannels = num => {
	const channels = ftdb.getMostActiveChannels(num);
	return channels.map(x => x.name);
}

const getCharCodes = s => {
	let charCodeArr = [];
	for (let i = 0; i < s.length; i++) {
		let code = s.charCodeAt(i);
		charCodeArr.push(code);
	}
	return charCodeArr;
}

const getUsersList = () => {
	const list = [];
	for (userKey in sessions) {
		const s = sessions[userKey];
		if (list.indexOf(s.user.nick) == -1) list.push(s.user.nick);
	};
	return list;
}

const getUserChannels = key => {
	const list = []
	const s = sessions[key];
	for (tabnum in s.tabs) {
		const tab = s.tabs[tabnum];
		//console.log(tab);
		if ((tabnum > 0) && (tab.name[0] != '@')) {
			list.push(tab.name);
		}
	}
	return list;
}

const getUsersOnChannelList = cname => {
	const list = [];
	for (userKey in sessions) {
		const s = sessions[userKey];
		if (s.tabs.filter(t => t.name == cname).length > 0) {
			if (list.indexOf(s.user.nick) == -1) list.push(s.user.nick);
		}
	};
	console.log(list);
	return list;
}


const parseOptVal = (v, old) => {
	if (v == 'X') { return old ^ 1 }
	if (Number(v) != NaN) { return Number(v) }
	return null;
}

const getStatus = userKey => {
	const uSession = sessions[userKey];
	uSession.user.lastActivity = ftdata.nowInSec();
	if (uSession) {
		uSession.tabs[0].lastMsg = uSession.msg.length;
		for (let t = 1; t < 5; t++) {
			const tab = uSession.tabs[t];
			if (tab) {
				if (tab.name[0] == '@') {
					uSession.tabs[t].lastMsg = ftdb.getPrivateLastMsg(uSession.user.id, tab.id);
				} else {
					uSession.tabs[t].lastMsg = ftdata.getChannelLastMsg(uSession, tab.id) || 0;
				}
			}
		}
		const forceTabOpen = uSession.forceTabOpen;
		uSession.forceTabOpen = CNST.NONE;
		return {
			tabCount: uSession.tabs.length,
			forceTabOpen,
			lastMsg: uSession.tabs.map(t => t.lastMsg),
			names: uSession.tabs.map(t => t.name),
		}
	}
	return false;
}

const buildBody = (token, data) => {
	let body = [token.charCodeAt(0)];
	switch (token) {

		case CNST.TOKEN_USER:
			body.push(bodyCardinal(data.id), bodyString(data.nick, 12), bodyString(data.key, 8));
			break;

		case CNST.TOKEN_ERROR:
			body.push(bodyByte(data.code), bodyString(data.msg));
			break;

		case CNST.TOKEN_STATUS:
			body.push(bodyByte(data.tabCount));
			body.push(bodyByte(data.forceTabOpen));
			for (let t = 0; t < 5; t++) {
				body.push((t < data.tabCount) ? bodyCardinal(data.lastMsg[t]) : bodyCardinal(0));
			};
			for (let t = 0; t < 5; t++) {
				body.push((t < data.tabCount) ? bodyString(data.names[t], 16) : bodyString('', 16));
			};
			break;

		case CNST.TOKEN_LINES:
			body.push(bodyByte(data.targetTab));
			body.push(bodyByte(data.lineCount));
			body.push(bodyCardinal(data.lastLine));
			for (let l = 0; l < data.lineCount; l++) {
				body.push(bodyLine(data.lines[l].body, 40));
			};
			break;

		default:
			break;
	}
	body = body.flat(2);
	//console.log(hexdump(Buffer.from(body)));
	return Buffer.from(body);
}

const wordFromStr = (s, num) => {
	const sarr = s.replace('  ', ' ').split(' ');
	return sarr[num];
}

const restFromStr = (s, num) => {
	const sarr = s.replace('  ', ' ').split(' ').slice(1);
	return _.join(sarr, ' ');
}


const sendTimeoutMessages = userKey => {
	const uSession = sessions[userKey];
	for (tnum in uSession.tabs) {
		tab = uSession.tabs[tnum];
		if ((tnum > 0) && (tab.name[0] != '@')) {
			let dbdata = ftdb.getChannelByName(tab.name);
			ftdata.addChannelMsg(uSession, {
				channelId: dbdata.id,
				fromId: null,
				fromName: '*',
				body: `user ${uSession.user.nick} has left (timeout).`
			});
		}
	}
}

const clearTimeoutedSessions = () => {
	const now = Math.floor(Date.now() / 1000);
	const timeouted = []
	for (userKey in sessions) {
		const session = sessions[userKey];
		if ((now - session.user.lastActivity) > config.SessionTimeout) {
			console.log(`timeouted key: ${userKey}`);
			//console.log(`ses:  ${session.lastActivity}    now: ${now}`);
			//console.log(session);
			timeouted.push(userKey);
		}
	}
	timeouted.forEach(userKey => {
		sendTimeoutMessages(userKey);
		delete (sessions[userKey])
	});

}



const scheduleCycle = () => {
	//console.log('* schedule cycle');
	clearTimeoutedSessions();
	//refreshChannelLastUpdate();
}

const startScheduler = () => {
	const scheduler = setInterval(scheduleCycle, config.SchedulerInterval);
}

const getSession = userKey => sessions[userKey];

const isAdmin = user => (user.role == CNST.ROLE_ADMIN);

const isNickLogged = nick => {
	for (s in sessions) {
		if (sessions[s].user.nick == nick) return true;
	}
	return false;
}

// ****************************************************************************************************************
// ****************************************************************************************************************
// ****************************************************************************************************************
// ****************************************************************************************************************

const sendCommand = (body, userKey, target) => {
	const uSession = getSession(userKey);
	const cmd = wordFromStr(body, 0);
	//console.log(`command: '${cmd}'`);
	switch (cmd) {
		case '/help': {
			let cname = wordFromStr(body, 1);
			uSession.msg.push(`----------------------------------------`);
			uSession.msg.push('  H E L P ' + ((cname) ? ` :: ${cname}` : ''));
			uSession.msg.push(`----------------------------------------`);
			if (!cname) {
				uSession.msg.push(...fujihelp.main);
			} else {
				if (fujihelp[cname]) {
					uSession.msg.push(...fujihelp[cname]);
				} else {
					uSession.msg.push(...fujihelp.default.map(l => l.replace('#', cname)));
				}
			}
			uSession.msg.push(`  `);
		}
			break;

		case '/keys': {
			uSession.msg.push(`----------------------------------------`);
			uSession.msg.push('  K E Y B O A R D    S H O R T C U T S  ');
			uSession.msg.push(`----------------------------------------`);
			uSession.msg.push(...fujihelp['shortcuts']);
		}
		break;

		case '/logout':
			ftdb.updateUserActivity(uSession.user.nick);
			delete (sessions[userKey]);
			break;

		case '/join':
		case '/j': {
			let cname = wordFromStr(body, 1);
			if (cname) { addTab(userKey, cname) }
		}
			break;

		case '/leave':
		case '/l':
		case '/part':
		case '/p': {
			let cname = wordFromStr(body, 1)
			if (!cname) {
				cname = uSession.tabs[target].name;
			}
			closeTab(userKey, cname);
		}
			break;

		case '/i':
		case '/info':
		case '/seen': {
			const nick = wordFromStr(body, 1);
			const user = ftdb.getUserByNick(nick);
			if (!user) {
				uSession.msg.push(`* User ${nick} unknown.`);
				break;
			}
			if (isNickLogged(nick)) {
				uSession.msg.push(`* User ${nick} is online.`);
				const channels = getUserChannels(user.key);
				if (channels.length == 0) {
					uSession.msg.push('* Not on any channel.');
				} else {
					uSession.msg.push(`* Currently on channels:`);
					uSession.msg.push(channels.join(', '));
				}
			} else {
				uSession.msg.push(`* User '${nick}' is offline.`);
				uSession.msg.push(`* Last seen ${user.lastActivity} UTC`);
			}
			break;
		}

		case '/list':
		case '/clist':
			uSession.msg.push(`* Most active channels:`);
			uSession.msg.push(getMostActiveChannels(10).join(', '));
			break;

		case '/priv':
			uSession.msg.push(`* Unread private messages:`);
			const ulist = getUnreadPrivateList(uSession.user.id);
			uSession.msg.push(ulist.length > 0 ? ulist.join(', ') : 'None.');
			break;

		case '/w':
		case '/who':
			uSession.msg.push(`* Users currently logged in:`);
			uSession.msg.push(getUsersList().join(', '));
			break;

		case '/version':
			uSession.msg.push(`* server version: ${CNST.VERSION}`);
			break;

		case '/removechannel':
			if (!isAdmin(uSession.user)) {
				uSession.msg.push(`* Forbidden`);
				break;
			} else {
				const cname = wordFromStr(body, 1);
				if (cname) {
					ftdb.purgeChannel(cname);
					if (ftdb.removeChannel(cname)) {
						uSession.msg.push(`* Channel ${cname} deleted.`);
					};
					closeAllSessionsTab(cname);
				}
			}
			break;

		case '/purgechannel':
			if (!isAdmin(uSession.user)) {
				uSession.msg.push(`* Forbidden`);
				break;
			} else {
				const cname = wordFromStr(body, 1);
				if (cname) {
					if (ftdb.purgeChannel(cname)) uSession.msg.push(`* Channel ${cname} purged.`);
				}
			}
			break;

		case '/broadcast':
		case '/servermsg':
			if (!isAdmin(uSession.user)) {
				uSession.msg.push(`* Forbidden`);
				break;
			} else {
				let msg = _.trim(restFromStr(body, 1));
				if (config.broadcastMsgTemplates) {
					if (config.broadcastMsgTemplates[msg]) msg = config.broadcastMsgTemplates[msg];
				}
				broadcast('* ' + msg);
			}
			break;

		case '/u':
		case '/ulist':
		case '/users': {
			cname = uSession.tabs[target].name;
			if ((target > 0) && (cname[0] != '@')) {
				uSession.msg.push(`* Users on channel ${cname}:`);
				uSession.msg.push(getUsersOnChannelList(cname).join(', '));
			} else {
				uSession.msg.push(`* Not here dude. Join channel first.`);
			}
		}
			break;

		case '/conf': {
			const option = wordFromStr(body, 1);
			const newvalue = wordFromStr(body, 2);
			if (!option) {
				uSession.msg.push(`* verbose ${uSession.user.showSrvMsg}`);
				break;
			}
			if (option == 'verbose') {
				const optval = parseOptVal(newvalue, uSession.user.showSrvMsg);
				if (optval != null) {
					uSession.user.showSrvMsg = optval;
					ftdb.setUserProp(uSession.user.id, 'showSrvMsg', optval);
				}
				uSession.msg.push(`* verbose ${uSession.user.showSrvMsg}`);
				break;
			}
			userError(userKey, CNST.ERROR_UNKNOWN_PARAMETER, `unknown parameter '${option}'`);
		}
			break;

		default:
			userError(userKey, CNST.ERROR_COMMAND_UNKNOWN, `Command unknown '${cmd}'`);
			break;
	}
}


// ****************************************************************************************************************
// ****************************************************************************************************************
// ****************************************************************************************************************
// ****************************************************************************************************************
// ****************************************************************************************************************


const sendMessage = (body, userKey, target) => {
	const uSession = getSession(userKey);
	const tab = uSession.tabs[target];
	const private = (tab.name[0] == '@');
	const msg = {
		fromId: uSession.user.id,
		fromName: uSession.user.nick,
		body
	}
	if (private) {
		msg.toId = tab.id;
		ftdb.addPrivateMsg(msg);
	} else {
		msg.channelId = tab.id;
		ftdata.addChannelMsg(uSession, msg);
	}
}

startScheduler();

module.exports = {
	isVersionOk,
	getSession,
	getStatus,
	getLinesData,
	loggedIn,
	loginUser,
	sendCommand,
	sendMessage,
	buildBody
}
