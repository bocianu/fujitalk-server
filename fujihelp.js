const who = [
    '* Usage:',
    '  ',
    '  /users',
    '  Shows all currently logged in users.',
    '  ',
    '* Alias: /w',
];

const users = [
    '* Usage:',
    '  ',
    '  /users',
    '  Displays a list of all users',
    '  on the current channel.',
    '  ',
    '* Alias: /ulist, /u',
];

const list = [
    '* Usage:',
    '  ',
    '  /list',
    '  Shows most active channels on the server.',
    '  ',
    '* Alias: /clist',
];

const leaveScreen = [
    '* Usage:',
    '  ',
    '  /leave <channel/@nick name>',
    '  Leaves specific channel/conversation.',
    '  It also closes corresponding tab.',
    '  ',
    '  /leave',
    '  Leaves current tab',
    '  ',
    '* Aliases: /l, /part, /p',
    '  ',
    '* Examples:',
    '  ',
    '  /l fujinet',
    '  /p @johnsmith',
    '  /leave'
];

const joinScreen = [
    '* Usage:',
    '  ',
    '  /join <channel name>',
    '  Joins a specific channel.',
    '  If the channel does not exist,',
    '  it will be created.',
    '  ',
    '  /join @<nickname>',
    '  Opens private conversation.',
    '  ',
    '* Alias: /j',
    '  ',
    '* Examples:',
    '  ',
    '  /j fujinet',
    '  /j @johnsmith',
];

const helpScreens = {
    "shortcuts": [ 
        ' ',
        '  START - switch to server tab',
        '  SELECT / TAB - switch to next tab',
        '  OPTION - change color theme',
        '  Ctrl u - scroll up',
        '  Ctrl d - scroll down',
        '  ESC - scroll back to bottom',
        '  Ctrl h - toggle verbose mode',
        '  Ctrl r - reload current tab',
        ' ',
     ],
    
    "main": [
        '* List of available commands:',
        '  ',
        'auth, conf, join, keys, leave, list,',
        'login, logout, priv, register, reload,',
        'seen, server, users, who, version',
        '  ',
        '* To get detailed info type:',
        '  /help [command name]',
        '  ',
        '* To get keyboard shortcuts list:',
        '  /keys',
    ],
    
    "auth": [      
        '* Usage:',
        '  ',
        '  /auth',
        '  Tries to authorize user using token',
        '  stored on FujiNet device (SD card).',
    ],
    
    "conf": [
        '* Usage:',
        '  ',
        '  /conf',
        '  Displays current configuration',
        '  ',
        '  /conf <name> <value>',
        '  Sets configuration option',
        '  ',
        '* Example:',
        '  ',
        '  /conf sioaudio 1',
        '  Sets sioaudio option to 1',
    ],
    
    "join": joinScreen,
    "j": joinScreen,
   
    "leave": leaveScreen,
    "l": leaveScreen,
    "part": leaveScreen,
    "p": leaveScreen,
    
    "keys": [
        '* Usage:',
        '  ',
        '  /keys',
        '  Shows keybord shortcuts list.',
    ],

    "clist": list,
    "list": list,

    "login": [
        '* Usage:',
        '  ',
        '  /login <username> <password>',
        '  Logs the user in to the server.',
        '  ',
        '* Example:',
        '  ',
        '  /login funkyBro p@$$p@$$',
    ],

    "logout": [
        '* Usage:',
        '  ',
        '  /logout',
        '  Logs the user out of the server.',
    ],

    "priv": [
        '* Usage:',
        '  ',
        '  /priv',
        '  Shows unread private messages.',
    ],

    "register": [
        '* Usage:',
        '  ',
        '  /register <username> <password>',
        '  Registers a new user on the server',
        '  and logs in.',
        '  ',
        '* Example:',
        '  ',
        '  /register funkyBro p@$$p@$$',
    ],

    "reload": [
        '* Usage:',
        '  ',
        '  /reload',
        '  Refreshes current tab and',
        '  clears server messages.',
        '  ',
        '* Shortcut: ctrl - R ',
    ],

    "seen": [
        '* Usage:',
        '  ',
        '  /seen <username>',
        '  Shows information about',
        '  the specified user.',
        '  ',
        '* Aliases: /info, /i',
        '  ',
        '* Example:',
        '  ',
        '  /i funkyBro',
    ],

    "server": [
        '* Usage:',
        '  ',
        '  /server',
        '  Displays current server',
        '  ',
        '  /server <address><:port>',
        '  Sets new server address',
        '  ',
        '* Example:',
        '  ',
        '  /server fujinet.pl:2137',
    ],    

    "users": users,
    "ulist": users,
    "u": users,

    "who": who,
    "w": who,

    "version": [
        '* Usage:',
        '  ',
        '  /version',
        '  Shows the current version of ',
        '  the client and the server.',
    ],

    "default": [
        '! Command # unknown,',
        '! or help page unavailable.',
    
    ],
}

module.exports = helpScreens;

