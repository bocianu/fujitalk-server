const Koa = require('koa');
const koaBody = require('koa-body');
const config = require('./config.js');
const fujitalk = require('./fujirouter.js');

const app = new Koa();
app.use(koaBody());
app.use(fujitalk.routes());
console.log('FujiTalk server started.');
app.listen(config.port);


