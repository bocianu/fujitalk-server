module.exports = {

    port: 2137,
    DB_file: 'fujitalk.db',
    SchedulerInterval: 5,       // sec
    SessionTimeout: 120,        // sec

    sqliteOptions: {
        verbose: console.log    
    },
    
    welcomeMsg: [
        '- Welcome to fujinet.pl server.',
        '- Have a nice chat!'
    ],

    broadcastMsgTemplates : {
        'restart': 'The Server will be restarted. BRB!',
        'down': 'The Server will be shut down            due to maintenance. BRB!',
        'update': 'The server will be updated. BRB!'
    },
    
}