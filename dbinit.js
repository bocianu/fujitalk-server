const config = require('./config.js');
const db = require('better-sqlite3')(config.DB_file, config.sqliteOptions);

db.prepare(`CREATE TABLE channelMsg (
    id INTEGER,
    fromId INTEGER,
    channelId INTEGER,
    fromName TEXT,
    msgTime TIMESTAMP,
    body TEXT,
    PRIMARY KEY("id" AUTOINCREMENT)
)`).run();

db.prepare(`CREATE TABLE channels (
    id INTEGER,
    name TEXT NOT NULL UNIQUE COLLATE NOCASE,
    lastMsg INTEGER,
    PRIMARY KEY("id" AUTOINCREMENT)
)`).run();

db.prepare(`CREATE TABLE directMsg (
    id INTEGER,
    fromId INTEGER,
    toId INTEGER,
    fromName TEXT,
    msgTime TIMESTAMP,
    unread INTEGER DEFAULT TRUE,
    body TEXT,
    PRIMARY KEY("id" AUTOINCREMENT)
)`).run();

db.prepare(`CREATE TABLE "users" (
	"id"	INTEGER,
	"key"	TEXT NOT NULL UNIQUE,
	"nick"	TEXT NOT NULL UNIQUE COLLATE NOCASE,
	"lastActivity"	TIMESTAMP,
	"pass"	TEXT NOT NULL,
	"role"	INTEGER NOT NULL DEFAULT 0,
	"showSrvMsg"	INTEGER NOT NULL DEFAULT 1,
	PRIMARY KEY("id" AUTOINCREMENT)
)`).run();

db.close();
