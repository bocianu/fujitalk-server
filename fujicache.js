var CNST = require('./constants');
const config = require('./config.js');
const ftdb = require('./ftdb.js');
const _ = require('lodash');

const nowInSec = () => Math.floor(Date.now() / 1000);

const cache = {
    users: [],
    channels: [],
};

const channel = {
    lastMsg: 0,
    messages: [],
    lastRefreshTime: null
};

const refreshChannelsLastMsg = () => {
	channels = ftdb.getChannelsLastUpdate();
	for (channel of channels) {
		ftdata.setChannelLastMsg(channel.id, channel.lastMsg);
	}
};

const getChannelLines = (session, cid, lastMsg) => {
    lastMsg = lastMsg==0xffffffff?-1:lastMsg;
    //getChannelLines(cid, lastMsg, session.user.showSrvMsg)
    const channel = getChannelById(session, cid);
    if (channel) {
        let lines = _.filter(channel.messages, m => ((m.channelId == cid) && (m.id > lastMsg) && (m.fromId || session.user.showSrvMsg)));
        lines = _.sortBy(lines, m => m.id);
        lines = _.reverse(lines)
        lines = _.take(lines, CNST.TAB_LINES);
        return lines;
    }
    return null;
    
        //const where = (showSrvMsg == 1)?'':'AND fromId IS NOT NULL'
        //return db.prepare(`SELECT * FROM channelMsg WHERE channelId = ? AND id > ? ${where} ORDER BY id DESC LIMIT ?`).all(cid, lastMsg, CNST.TAB_LINES);
        //channelId: dbdata.id,
        //fromId: null,
        //fromName: '*',
        //body: `user ${uSession.user.nick} has left (timeout).`

};

const getChannelById = (session, cid) => {
    if (!_.some(cache.channels, ['id', cid])) {
        const channel = ftdb.getChannelById(cid);
        if (!channel) return null;
        channel.messages = ftdb.getChannelLines(cid, 0, session.user.showSrvMsg); 
        cache.channels.push(channel);
    } 
    return _.find(cache.channels, ['id', cid]);
};

const getChannelLastMsg = (session, cid) => {
    const channel = getChannelById(session, cid);
    if (channel) return channel.lastMsg;
    return null;
};

const setChannelLastMsg = (session, cid, lastMsg) => {
    const channel = getChannelById(session, cid);
    if (channel) return channel.lastMsg = lastMsg;
    return null;
};

const addChannelMsg = (session, msg) => {
    const lastMsg = ftdb.addChannelMsg(msg);
    const cid = msg.channelId;
    const dbmsg = ftdb.getChannelMsg(lastMsg);
    const channel = getChannelById(session, cid);
    if (channel) {
        channel.messages.push(dbmsg);
        setChannelLastMsg(session, cid, lastMsg);
        return lastMsg;
    } 
    return null
};

module.exports = {
    nowInSec,
    addChannelMsg,
    setChannelLastMsg,
    getChannelLastMsg,
    getChannelLines,
};